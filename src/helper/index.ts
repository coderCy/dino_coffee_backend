import crypto from 'crypto';

const SECRET = 'TIME-WILL-PROVE';

export const random = () => crypto.randomBytes(16).toString('hex');
export const authentication = (salt: string, password: string) => {
    return crypto.createHmac('sha256', salt).update(password).update(SECRET).digest('hex');
};



