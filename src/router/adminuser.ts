import express from 'express';

import { deleteAdminUser, getAllAdminUser, updateAdminUser } from '../controllers/adminuser';

import { isAuthenticated,isAdminOwner } from '../middlewares/index';

export default (router: express.Router) => {    
     router.get('/adminuser', isAuthenticated,getAllAdminUser);
     router.delete('/adminuser/:id', isAuthenticated,isAdminOwner,deleteAdminUser);
     router.patch('/adminuser/:id', isAuthenticated,isAdminOwner,updateAdminUser); 
    };


