import express from 'express';

import authentication from './authentication'; 
import adminuser from './adminuser';

const router = express.Router();

//The authentication module is imported, which is expected to export a function that accepts an Express router as a parameter.
export default (): express.Router => {   
    authentication(router);
    adminuser(router);
    return router;
}