import express from 'express';
import { deleteAdminUserById, getAdminUser, getAdminUserById, updateAdminUserById } from '../models/users'

export const getAllAdminUser = async (req: express.Request, res: express.Response) => {
  try{
     const adminUser = await getAdminUser();

     return res.status(200).json(adminUser);

  }catch(error){
    console.log(error);
    return res.sendStatus(400);
  }
}

export const deleteAdminUser = async (req: express.Request, res: express.Response) => {   
  try{
    const {id} = req.params;
    
    const deleteAdminUser = await deleteAdminUserById(id);
    
    return res.json(deleteAdminUser);

  }catch(error){
  console.log(error);
    return res.sendStatus(400);
  }
}

export const updateAdminUser = async (req: express.Request, res: express.Response) => {
    try{
      const {id} = req.params;  

      const {adminusername} = req.body;

      if(!adminusername){
        return res.sendStatus(400);
      }
      
      const existingAdminUser = await getAdminUserById(id);
      existingAdminUser.adminName = adminusername;
      await existingAdminUser.save();

      return res.status(200).json(existingAdminUser).end();
       
    }catch(error){
       console.log(error);
      return res.sendStatus(400);
    }
}
