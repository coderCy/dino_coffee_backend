import express from 'express';
import { createAdminUser, getAdminUserByEmail } from '../models/users';
import LogicException from '../Exception/logicException';
import { authentication, random } from '../helper/index';

// Object to store failed attempts per IP address
const failedAttempts: any = {};

export const login = async (req: express.Request, res: express.Response) => {
  try{

    const {email, password} = req.body;
    
    if (!email || !password){
        throw new LogicException('Invalid password or email');
    }   
    
    const adminUser = await getAdminUserByEmail(email).select('+authentication.salt +authentication.password');

    if(!adminUser){
        throw new LogicException('Invalid user');
    }


    const expectedHash = authentication(password, adminUser.authentication.salt);

    // if the password is incorrect
    if (adminUser.authentication.password !== expectedHash){
        throw new LogicException('Invalid password');
    }   

    const salt = random();
    adminUser.authentication.sessionToken = authentication(salt, adminUser._id.toString());

    await adminUser.save();
    res.cookie('TIME-WILL-PROVE',adminUser.authentication.sessionToken, { domain: 'localhost', path: '/' });
    return res.status(200).json(adminUser).end();
  }

  catch(error){
    console.log(error); 
    return res.sendStatus(400);
  }
}

export const register = async (req: express.Request, res: express.Response) => {
    try{
    
    //#region  User Checking    
    const {email, password, adminName} = req.body;

    if (!email || !password || !adminName){
        throw new LogicException('Invalid user');
    }   
    const existingAdmin = await getAdminUserByEmail(email);

    // if the user already exists
    if (existingAdmin){
        throw new LogicException('Admin already exists');
    }
    //#endregion
    
    //#region  Create User 
    const salt = random();
    const adminUser = await createAdminUser({
        email, 
        adminName,
        authentication : {
            salt,
            password : authentication(password, salt)
        }
    });

    return res.status(200).json(adminUser).end();    
    //#endregion 

    }catch(error){
     console.log(error); 
     return res.sendStatus(400);
    }
 }

 // Middleware function to check if the using type the wrong password 3 times

/*dasdasdasdasaa
const blobkLoginAfterThreeFailedAttempts = 
(req: express.Request, res: express.Response, next: express.NextFunction) => {
    const ip = req.ip;
    if(!failedAttempts[ip]){
        failedAttempts[ip] = 0;
    }
    failedAttempts[ip]++;
    if(failedAttempts[ip] > 3){
        
        // block the login
        return res.status(403).send('Login blocked you may contact the super user');
    }
    next();
   
     const {email, password, adminName} = req.body;
     // help me declare a bool variable is called isValidUser for declare email , password and username is correct or not
     let isValidUser = true;
     // check if email is valid or not
     if(!email || !password || !adminName){
         isValidUser = false;
     }

     if (!isValidUser){
        const ip = req.ip;
        failedAttempts[ip] = 0;
        failedAttempts[ip]++;
        
        if (failedAtt empts[ip] == 3){
            return res.status(403).send('Login blocked you may contact the super user');
        }else{
            return res.status(400).send('Invalid user');
        }   
    }
   
     }catch(error){
     console.log(error); 
     return res.sendStatus(400);
    }



}
*/
