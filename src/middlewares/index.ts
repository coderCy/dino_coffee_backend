import express from 'express';
import {get, merge} from 'lodash'; 

import { getAdminUserBySessionToken } from '../models/users';

export const isAdminOwner = async (req: express.Request, res: express.Response, next: express.NextFunction) => {    
   try{

   const {id} = req.params;
   const currentAdminUserId = get(req, 'identity._id') as string;

   if(!currentAdminUserId){
    return res.sendStatus(403);
   }

   if (currentAdminUserId.toString() !== id){
    return res.sendStatus(403);
   }      
   
   next();

   }catch(error){
    console.log(error);
    return res.status(400);
   }
}

export const isAuthenticated = async (req: express.Request, res: express.Response, next: express.NextFunction) => { 
  try{
    const sessionToken = req.cookies['TIME-WILL-PROVE'];
   if(!sessionToken){
    return res.sendStatus(403);
   }

   const existingAdmin = await getAdminUserBySessionToken(sessionToken);

   if(!existingAdmin){
    return res.sendStatus(403);
   }

   merge(req, {identity: existingAdmin})

   return next();

  }catch(error){
    console.log(error);
    return res.status(400);
  }
}
