import mongoose from "mongoose";


// For Admin Portal
const AdminSchema = new mongoose.Schema({  
    adminName: {type: String, required: true},
    email: {type: String, required: true},
    authentication : {
        password: {type: String, required: false},   
        salt: {type: String, required: false},
        sessionToken: {type: String, required: false}   
    }
});

export const AdminUserModel = mongoose.model('AdminUser', AdminSchema);

export const getAdminUser = () => AdminUserModel.find();
export const getAdminUserByEmail = (email: string) => AdminUserModel.findOne({email}); (id: string) => AdminUserModel.findById(id);
export const getAdminUserBySessionToken = (sessionToken: string) => AdminUserModel.findOne({
    authentication: {sessionToken}
});
export const getAdminUserById  = (id: string) => AdminUserModel.findById(id);
export const createAdminUser = (values: Record<string, any>) => new AdminUserModel(values)
.save().then((user) => user.toObject());  
export const deleteAdminUserById = (id: string) => AdminUserModel.findOneAndDelete({_id: id});
export const updateAdminUserById = (id: string, values: Record<string, any>) => AdminUserModel.findOneAndUpdate({_id: id}, values);