class LogicException extends Error {
    statusCode: number;
    
    constructor(message: string) {
        super(message);
        this.statusCode = 417;
    }
}

export default LogicException;