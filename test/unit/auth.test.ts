import { register } from '../../src/controllers/authentication';
import {
  createAdminUser,
  getAdminUserByEmail,
} from '../../src/models/users';

jest.mock('../../src/models/users', () => ({
  createAdminUser: jest.fn(),
  getAdminUserByEmail: jest.fn(),
}));

describe('Authentication Controller', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should not register a new admin user if user already exists', async () => {
    // Mock the getAdminUserByEmail function to return an existing user
    const existingUser = { email: 'test@example.com' };
    (getAdminUserByEmail as jest.Mock).mockResolvedValue(existingUser);

    const req = {
      body: {
        email: 'test@example.com',
        password: 'password123',
        adminName: 'John Doe',
      },
    };
    const res = { sendStatus: jest.fn() };

    await register(req as any, res as any);

    expect(getAdminUserByEmail).toHaveBeenCalledTimes(1);
    expect(createAdminUser).not.toHaveBeenCalled();
    expect(res.sendStatus).toHaveBeenCalledWith(400);
  });
});
